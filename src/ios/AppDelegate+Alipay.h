//
//  AppDelegate+Wechat.h
//  cordova-plugin-wechat
//
//  Created by Jason.z on 26/2/20.
//
//

#import "AppDelegate.h"

@interface AppDelegate (alipay)

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

@end
