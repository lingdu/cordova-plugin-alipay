/********* alipay.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <AlipaySDK/AlipaySDK.h>


@interface alipay : CDVPlugin

@property (nonatomic, strong) NSString *currentCallbackId;
@property (nonatomic, strong) NSString *alipayAppId;

- (void)payment:(CDVInvokedUrlCommand*)command;
- (BOOL)handleAlipayOpenURL:(NSURL *)url;
@end

