//
//  AppDelegate+Wechat.h
//  cordova-plugin-wechat
//
//  Created by Jason.z on 26/2/20.
//
//

#import "AppDelegate+Alipay.h"
#import "alipay.h"

#import <objc/runtime.h>

@implementation AppDelegate (alipay)

static BOOL swizzled = NO;
+ (void)load {
    NSProcessInfo *processInfo = [NSProcessInfo processInfo];
    if ([processInfo respondsToSelector:@selector(operatingSystemVersion)]) {
        //operatingSystemVersion was introduced in iOS 8
        Method swizzlee = class_getInstanceMethod(self, @selector(application:continueUserActivity:restorationHandler:));
        Method swizzler = class_getInstanceMethod(self, @selector(swizzleApplication:continueUserActivity:restorationHandler:));
        
        if (swizzlee) {
            method_exchangeImplementations(swizzlee, swizzler);
            swizzled = YES;
        } else {
            // app delegate has not implemented optional protocol method. add it in with our implementation
            const char *typeEncoding = method_getTypeEncoding(swizzler);
            class_addMethod(self, @selector(application:continueUserActivity:restorationHandler:), method_getImplementation(swizzler), typeEncoding);
        }
    }
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    alipay *cdvAlipay = [self.viewController getCommandInstance:@"alipay"];
    return [cdvAlipay handleAlipayOpenURL:url];
}



@end
